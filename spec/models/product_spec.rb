require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }

    it "are match related product" do
      expect(product.related_products).to match_array(related_product)
    end
  end
end
