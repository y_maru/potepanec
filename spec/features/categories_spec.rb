require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  scenario "show projects which could render a single product" do
    visit potepan_category_path(taxon.id)
    click_on "#{product.name} image"
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
