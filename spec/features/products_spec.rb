require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }
  let(:notaxon_product) { create(:product) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  context "with taxon" do
    before do
      visit potepan_product_path(product.id)
    end

    scenario "links to category page" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
    scenario "links to other relational product" do
      expect(page).to have_content "#{related_product.name}"
      expect(page).to have_content "#{related_product.display_price}"
      click_on "#{related_product.name} image"
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end

  context "without taxon" do
    before do
      visit potepan_product_path(notaxon_product.id)
    end

    scenario "don't view links to category page" do
      expect(page).not_to have_content '一覧ページへ戻る'
    end
  end
end
