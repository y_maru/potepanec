require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxon) { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "responds successfully" do
    expect(response).to be_success
  end
  it "has taxons name" do
    expect(response.body).to include taxon.name
  end
  it "has taxonomies name" do
    expect(response.body).to include taxonomy.name
  end
  it "has products name" do
    expect(response.body).to include product.name
  end
end
