require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "GET /show" do
    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(200)
    end
    it "has product info" do
      expect(response.body).to include(product.name)
      expect(response.body).to include(product.description)
      expect(response.body).to include(product.display_price.to_s)
    end
    it "has related product info" do
      expect(response.body).to include(related_products[0].name)
      expect(response.body).to include(related_products[0].display_price.to_s)
    end
    it "views limited number of related products" do
      expect(Capybara.string(response.body)).to have_selector ".productBox", count: 4
    end
  end
end
